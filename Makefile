ANSIBLE_USER ?= ubuntu## ansible user for the playbooks
CLUSTER_KEYPAIR ?= ska-techops## key pair available on openstack to be put in the created VMs
DEBUG ?= false
CONTAINERD ?= true
NVIDIA ?= false
IGNORE_NVIDIA_FAIL ?= false
TAGS ?= all
COLLECTIONS_PATHS ?= ./collections
PODMAN_REGISTRY_MIRROR ?= dockerhub.stfc.ac.uk
REGISTRY_MIRROR ?= https://$(PODMAN_REGISTRY_MIRROR)
V ?=
THIS_BASE=$(shell pwd)

# used for determining redirection to git submodules
TARGET_CMD=$(shell echo $(MAKECMDGOALS) | cut -d ' ' -f1)
TARGET_ARGS=$(shell echo $(MAKECMDGOALS) | cut -d ' ' -f2-)

.DEFAULT_GOAL := help

.IGNORE: build elasticstack_build build_logging ceph build_stack

.PHONY: k8s help

## Bifrost args
BIFROST_VARS ?= bifrost_vars.yml
BIFROST_CLUSTER_INVENTORY ?= ../inventory_bifrost
# BIFROST_RUN_TAGS ?= ansible,jumphost,openstack,k8s,ceph
BIFROST_RUN_TAGS ?= ansible,jumphost,dns,openstack,k8s,
BIFROST_CLUSTER_NAME ?= terminus
BIFROST_EXTRA_VARS ?= registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)' jump_host=' -F $(THIS_BASE)/ssh.config $(BIFROST_CLUSTER_NAME) '
KUBECONFIG_FILE ?="/etc/stfc_k8s_config"
RESCUE_PASSWORD ?= ""

## ElsticStack args
ELASTICSTACK_VARS ?= elasticstack_vars.yml
LOGGING_NODES ?= all
LOGGING_INVENTORY_FILE ?= combined_inventory

## K8s args
K8S_VARS ?= k8s_vars.yml
K8S_CLUSTER_INVENTORY ?= inventory_k8s
K8S_EXTRA_VARS ?=

# define overides for above variables in here
-include PrivateRules.mak

# collections and roles locations
ANSIBLE_COLLECTIONS_PATHS := $(COLLECTIONS_PATHS):~/.ansible/collections:/usr/share/ansible/collections
DOCKER_PLAYBOOKS := $(COLLECTIONS_PATHS)/ansible_collections/ska_cicd/docker_base/playbooks
DOCKER_ROLES := $(COLLECTIONS_PATHS)/ansible_collections/ska_cicd/docker_base/roles/docker
STACK_CLUSTER_PLAYBOOKS := $(COLLECTIONS_PATHS)/ansible_collections/ska_cicd/stack_cluster/playbooks
K8S_PLAYBOOKS := $(COLLECTIONS_PATHS)/ansible_collections/ska_cicd/k8s/playbooks

vars:  ## Variables
	@echo "Current variable settings:"
	@echo "NODE_NAME=$(NODE_NAME)"
	@echo "NODE_VARS=$(NODE_VARS)"
	@echo "CLUSTER_KEYPAIR=$(CLUSTER_KEYPAIR)"
	@echo "INVENTORY_FILE=$(INVENTORY_FILE)"
	@echo "BIFROST_EXTRA_VARS=$(BIFROST_EXTRA_VARS)"
	@echo "ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS)"
	@echo "STACK_CLUSTER_PLAYBOOKS=$(STACK_CLUSTER_PLAYBOOKS)"
	@echo "DOCKER_PLAYBOOKS=$(DOCKER_PLAYBOOKS)"
	@echo "DEBUG=$(DEBUG)"

set_inventory:
	mkdir -p combined_inventory
	rm -f combined_inventory/*
	cp inventory_* combined_inventory/
	rm -f combined_inventory/*.save combined_inventory/*.backup
	cat combined_inventory/inventory_* > all_inventory

k8s_nodes: ## Kubernetes Build nodes based on heat-cluster
	[ ! -e $(K8S_VARS) ] || touch $(K8S_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/cluster-infra.yml \
					 -e @$(THIS_BASE)/$(K8S_VARS) \
					 --extra-vars="$(K8S_EXTRA_VARS)" \
					 --extra-vars="cluster_keypair='$(CLUSTER_KEYPAIR)'" \
					 --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					 --extra-vars="jump_host=' -F $(THIS_BASE)/ssh.config $(BIFROST_CLUSTER_NAME) '" \
					 --extra-vars="inventory_file=../../../../$(K8S_CLUSTER_INVENTORY)" $(V)

k8s_common:  ## Kubernetes apply the common roles
	[ ! -e $(K8S_VARS) ] || touch $(K8S_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(K8S_CLUSTER_INVENTORY) $(STACK_CLUSTER_PLAYBOOKS)/common.yml \
					  -e @$(THIS_BASE)/$(K8S_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					  --extra-vars="$(K8S_EXTRA_VARS)" $(V)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(K8S_CLUSTER_INVENTORY) $(DOCKER_PLAYBOOKS)/docker.yml \
					  -e @$(THIS_BASE)/$(K8S_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					  --extra-vars="$(K8S_EXTRA_VARS)" $(V)

k8s_build: ## all the k8s node setup steps
	# make k8s_nodes  # cannot do k8s_nodes anymore because of VM image
	make k8s_common
	make build_haproxy
	make build_k8s

k8s_tell_next_worker:
	openstack server list --name="systems-k8s1-worker*" -f value -c Name | \
	python3 -c "import sys; m=[int(line.strip().split('-').pop()) for line in sys.stdin]; print('run: make k8s_add_worker WORKER=systems-k8s1-worker-'+str(sorted(m).pop()+1))"

k8s_add_worker: ## add a new worker - pass: WORKER=systems-k8s1-worker-N
	@if [ "" = "$(WORKER)" ]; then \
	echo "WORKER not set, use: 'WORKER=systems-k8s1-worker-N' - abort!"; exit 1; fi
	make k8s_nodes K8S_VARS=k8s_worker_vars.yml K8S_EXTRA_VARS='{"cluster_name": "$(WORKER)", "genericnode_worker": {"name": "$(WORKER)", "flavor": "le2.small", "image": "ubuntu-bionic-18.04-nogui", "num_nodes": 1}}' K8S_CLUSTER_INVENTORY=inventory_k8s_$(WORKER)
	make k8s_common K8S_VARS=k8s_worker_vars.yml K8S_EXTRA_VARS= K8S_CLUSTER_INVENTORY=inventory_k8s_$(WORKER)
	# do the join !!!!!!
	mkdir -p k8s_combined_inventory
	rm -f k8s_combined_inventory/*
	cp $(K8S_CLUSTER_INVENTORY) k8s_combined_inventory/
	cp inventory_k8s_$(WORKER) k8s_combined_inventory/
	make k8s_distribute_kubeconfig
	make build_k8s K8S_VARS=k8s_worker_vars.yml K8S_CLUSTER_INVENTORY=k8s_combined_inventory \
	 TAGS="--limit $(WORKER),systems-k8s1-master-0,systems-k8s1-loadbalancer --tags k8s,join"

help:  ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
production: false
