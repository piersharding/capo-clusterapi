# capo-clusterapi

## SYNOPSIS

This repository is a collection of experiments that package the process of deploying Kubernetes cluster using [clusterapi](https://cluster-api.sigs.k8s.io/) using the OpenStack infrastructure provider - https://github.com/kubernetes-sigs/cluster-api-provider-openstack .

## Versions

This example is using clusterapi v1.2.1, CAPO v0.6.3, Kubernetes 1.24.3.

## Setup

Create a management cluster

Obtain the clouds.yaml file

Get the Ceph admin connection details

Set PrivateRules.mak values

```
# OpenStack External Network ID on this tenant
OPENSTACK_EXTERNAL_NETWORK_ID = 5283f642-8bd8-48b6-8608-fa3006ff4539
# DNS server to use inside Kubernetes workload cluster
OPENSTACK_DNS_NAMESERVERS = 192.168.99.194
# OpenStack clouds yaml config file
CAPO_CLOUDS_PATH = ../clouds-skatechops.yaml
# tenant connection details inside cloud yaml file
CAPO_CLOUD = skatechops
# path to ceph admin config - contents of a /etc/ceph
ETC_CEPH = $(THIS_BASE)/../ceph
```

Generate cluster manifest

