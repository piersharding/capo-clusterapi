#!/usr/bin/bash

echo "Running the cluster initialisation: Ingress"
export NGINX_VERSION=${NGINX_VERSION}
export KUBECONFIG=/etc/kubernetes/admin.conf

#set -e
set -x
echo "Deploy NGINX Ingress"
kubectl \
apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v$NGINX_VERSION/deploy/static/provider/baremetal/deploy.yaml
echo "Patch ports to 30080, 30443"
kubectl \
-n ingress-nginx patch service/ingress-nginx-controller \
-p '{"spec": {"ports": [{"port": 80, "nodePort": 30080}, {"port": 443, "nodePort": 30443 } ] } }'
echo "Patch class name for controller to nginx"
kubectl \
-n ingress-nginx patch deployment/ingress-nginx-controller \
--type='json' -p='[{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "--ingress-class=nginx"}]'
echo "patch IngressClass to be default"
kubectl \
patch ingressclass/nginx \
-p='{"metadata": {"annotations": {"ingressclass.kubernetes.io/is-default-class": "true"}}}'

curl -s https://gitlab.com/piersharding/capo-clusterapi/-/raw/master/cluster/resources/ingress-nginx-acls.yaml | \
envsubst  | kubectl apply -f -

kubectl -n ingress-nginx apply -f - <<EOF
apiVersion: v1
kind: Service
metadata:
  labels:
    app.kubernetes.io/component: controller
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/part-of: ingress-nginx
    app.kubernetes.io/version: ${NGINX_VERSION}
  name: ingress-nginx-controller-lb
  annotations:
    service.beta.kubernetes.io/openstack-internal-load-balancer: "true"
spec:
  ports:
  - appProtocol: http
    name: http
    port: 80
    protocol: TCP
    targetPort: http
  - appProtocol: https
    name: https
    port: 443
    protocol: TCP
    targetPort: https
  selector:
    app.kubernetes.io/component: controller
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/name: ingress-nginx
  type: LoadBalancer
EOF

echo "Finished."
exit 0
